# Условия работы программы "calculator":
# 1. Убираем все пробелы
# 2. Убираем все пустые скобки
# 3. Много точек заменяем одной, числа вида: .5 и 5. питон прекрасно умеет обрабатывать сам, как оказалось
# 4. Множественные минусы и плюсы обрабатываем по правилам алгебры
# 5. Множественное деление игнорируем, воспринимаем как одинарное
# 6. Множественное умножение считаем возведением в степень
# 7. Пусть числа будут не огромными, выражение вида: 3.3e+73 обрабатывать пока что не умеем (написать легко, но потом)
# 8. Долгожданная реализация тригонометрических функций (описание и небольшой бонус в соответствующем файле)

from removing_unnecessary import removing_unnecessary_operators
from exceptions import find_exceptions
from calculate import calculation_expression
from utilits import adding_multiplication_near_parentheses
from trigonometric_functions import calculating_functions


def calculator(expression: str):
    expression = str(expression)
    # удалим всё лишнее (подробности в файле removing_unnecessary)
    expression = removing_unnecessary_operators(expression)
    # посмотрим, нет ли там ошибки
    if expression.find('EXC') != -1:
        return expression
    # проведем проверку выражения на некорректный ввод
    find_exc = find_exceptions(expression)
    if find_exc != None:
        return find_exc
    # print(expression)
    # добавим недостающее
    expression = adding_multiplication_near_parentheses(expression)
    # print(expression)
    # вначале вычислим наши функции
    expression = calculating_functions(expression)
    if expression.find('EXC') != -1:
        return expression
    # вычисляем выражение
    expression = calculation_expression(expression)
    # возможно в процессе вычисления возникли ошибки
    if expression.find('EXC') != -1:
        return expression
    return float(expression)


def main():
    print(f'2 * 2 = {int(calculator(" 2 * 2 "))}')


main()
