from tkinter import *
from tkinter.ttk import Combobox
from main import calculator


def clicked():
    expression = txt1.get()
    Label(window, text=f"Answer: {calculator(expression)}", font=('Times New Roman', 20), bg='red', fg='white').grid(
        column=0, row=5)


window = Tk()
window.geometry('607x500')
window.title('Calculator')
lbl = Label(window, text="It's a very cool calculator", font=('Times New Roman', 25))
lbl.grid()

Label(window, text='Enter your expression: ', font=('Times New Roman', 25)).grid(column=0, row=1)
txt1 = Entry(window, width=75, )
txt1.grid()

bttn = Button(window, text='Press to calculate', font=('Times New Roman', 25),
              bg='white', fg='purple', command=clicked, width=20, height=3)
bttn.grid(column=0, row=4)

window.mainloop()
